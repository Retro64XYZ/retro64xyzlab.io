---
layout: post
title: "Using YouTube Over Gopher"
date: 2018-09-08T04:52:43-07:00
author: RetroMe
summary: >
  The gopher protocol can provide access to the website YouTube. Here is how. 
categories: how-to
thumbnail: fa-desktop
tags:
 - gopher
 - port 70
 - youtube
 - how to
---

# Gopher And YouTube

[Gopher][gopherprotocol] is an awesome protocol designed for distributing,
searching, and retrieving files and documents over the internet. Gopher is
purposefully oriented towards a menu based design and is an alternative to the
World Wide Web. Some may consider Gopher part of the 'dark web' or the internet
that is rarely or never crawled by search engines. Others just think Gopher is
out of date and deprecated. I think they are wrong.

Gopher is amazing but one of the major complaints that people have is the lack
of modern amenities provided by the likes of the World Wide Web proper.
However, the folks at [shroom party][shroomparty] have devised a method to
integrate YouTube with Gopher and even offer rudimentary searching. This is
amazing!

## Example

![YouTubeOverGopher][tubegif]

## How Do They Do It?

The below code accomplishes the YouTube to Gopher translation. They are able to
kick off python scripts from their Gopher server and are able to create a
gopher site they serve on the fly from an API connection they have with
YouTube. Check out their [git][shroomcode]!

### search.dcgi

```
1 #!/usr/bin/env python3
2 import sys
3 import yapi
4
5 def main(kw):
6 	list = []
7
8 	# query from api
9 	with open('/home/gopher/.ytapikey', 'r') as keyfile:
10 		key = keyfile.read().strip()
11 	api = yapi.YoutubeAPI(key)
12 	results = api.general_search(kw, max_results=10)
13 	for r in results.items:
14 		if r.id.kind == 'youtube#video':
15 			res = {"title": r.snippet.title, "id": r.id.videoId, "channel": r.snippet.channelTitle, "url": f"https://www.youtube.com/watch?v={r.id.videoId}"}
16 			list.append(res)
17
18 	# print
19 	print('tresults for: '+kw)
20 	print()
21 	for r in list:
22 		print(f"[i|{r['channel']} {r['title']}||server|port]")
23 		print(f"[h|{r['url']}|URL:{r['url']}|server|port]")
24 		print(f"[9|download|/yt/dl.cgi?{r['id']}|server|port]")
25 		print()
26
27 main(sys.argv[1])
```

### dl.cgi
```
1 #!/bin/bash
2 youtube-dl --cache-dir /tmp/ytcache -q "https://hooktube.com/watch?v=$2" -o - 2>/dev/null
```

### index.gph
```
[7|search|/yt/search.dcgi|server|port]
```

[shroomparty]: gopher://shroom.party:70 'The gopher site for the shroom party'
[gopherprotocol]: https://en.wikipedia.org/wiki/Gopher_(protocol) 'The Gopher protocol'
[lynxbrowser]: https://en.wikipedia.org/wiki/Lynx_(web_browser) 'The Lynx Browser'
[tubegif]: /../assets/images/gifs/09082018-gopher-and-youtube/youtube.gif 'Animated Display Of YouTube Over Gopher'
[shroomcode]: https://code.shroom.party/gopher/file/README.html 'The shroom party git for the Gopher Code'
