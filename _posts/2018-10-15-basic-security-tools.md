---
layout: post
title: "UAT Talk"
date: 2018-10-12T04:52:43-07:00
author: RetroMe
summary: >
  What are some basic cyber security related behaviors and tools we can
  implement in order to better defend ourselves? I discuss a few of them.
categories: software
thumbnail: fa-microchip
tags:
 - software
 - arp
 - firejail
 - clamav
 - dns
 - basics
 - how to
---

# Introduction

There are several things we can do to better enhance our security. We will
discuss software as well as behaviors that can make us safer. This post
consists of notes used and presented during my University Of Advancing
Technology talk. This post was intended for individuals to follow along with.

## Anti-Virus

Anti-Virus in Linux? Yes! We should consider using AV even if we are using
Linux. ClamAV is an excellent choice. Being able to scan files for infection is
important because we do not live in a world where we can completely ignore
other operating systems.

```
$ sudo pacman -S clamav
$ sudo freshclam
$ sudo systemctl start clamav-daemon.service
$ sudo systemctl enable clamav-daemon.service
$ yaourt -S clamav-unofficial-sigs
```

## OpenVPN

Find a good VPN service and use it. Just remember your threat profile and
assume your VPN is being monitored.

1. [Proton VPN][protonvpn]
2. [Nord VPN][nordvpn]
3. [Private Internet Access VPN][piavpn]

Pick one from the list or find one you like. Later you may decide to deploy
your own.

## Tor, I2P, Freenet

Tor may be necessary depending on your threat profile and what you are try to
accomplish. Freenet and I2P may also be viable alternatives depending on your
needs.

1. [Tor][tor]
2. [I2P][i2p]
3. [Freenet][freenet]

## ArpON

Use the tool [ArpON][arpon] to defend yourself from ARP spoofing/poisoning
attacks. This is useful for those folks who travel and may find themselves on
foreign networks with many untrusted users.

```
$ yaourt -S arpon-ng
```

Setup will vary for the type of network you plan to be a participant on.

## Firewalls & UFW

A firewall can and should be used at home and is of particular importance when
using a laptop on foreign networks. [UFW][ufwarchwiki] is easy to use and setup.
A good example of installation and configuration for home use is below.

```
$ yaourt -S ufw
$ sudo systemctl start ufw.service
$ sudo systemctl enable ufw.service
$ sudo ufw default deny incoming
$ sudo ufw default allow outgoing
$ sudo ufw allow ssh
$ sudo ufw enable 
```

You can [learn more here][ufwdo] about UFW.

## Firejail

Firejail is an excellent tool for sandboxing applications like your web browser.

```
$ yaourt -S firejail
```

You can then run `firejail chromium` and this will use the default settings for
sandboxing your browser application. There exist many default profiles for well
known applications. You can also build your own profiles. This is a must have.

## AppArmor

A Mandatory Access Control system goes far beyond the normal Discretionary
Access Control system provided by Linux. AppArmor has excellent default
profiles for many applications and is relatively simple to setup depending on
your choice of distribution. I am of the opinion that the simplicity of
AppArmor in combination with Firejail is superior to SELinux and provides
excellent coverage against most issues you might run into.

Learn more about AppArmor at the [Arch Wiki][apparmorarchwiki].

## GnuPG

Learn how to encrypt and sign data and messages. [GnuPG][gnupg] is an excellent
tool for this use.

```
$ # Generate a Key
$ gpg --gen-key
$ # Generate a revocation Cert
$ # Make One For Several Eventualities
$ gpg --output ~/revocation.crt --gen-revoke your_email@address.com
# Make it inaccessible to others
$ chmod 600 ~/revocation.crt
```

Now that you have a key generated you can learn more about using GnuPG at
[Digital Ocean][gnupgdo] or the [Arch Wiki][gnupgarchwiki].

## OpenNIC

Check out the [OpenNIC project][opennic]. Your DNS is a weak link. Learn to use
an alternative from the one issued to you through your provider.

You can combine this with [DNSCrypt][opencrypt].

You can diagnose DNS issues with [dnsdiag][dnsdiag].

## Password Manager

Pick one. There are tons. You should be using two factor authentication and
strong passwords every where.

1. [SAASPASS][saaspass]
2. [KeePass][keepass]

## GNUzilla and IceCat

The [IceCat][icecat] web browser gives you better control over your privacy by
allowing you to disable JavaScript where necessary.

# Conclusion

The internet provides many ways by which we can reduce our safety, privacy, and
security. Some of the options above can be used to help claw back some of your
digital rights. You as a user are responsible for your own destiny. You can
defend yourself by adding basic software and securing focused practices to your
life style.

[arpon]: https://en.wikipedia.org/wiki/ArpON 'A ARP protection application'
[ufwarchwiki]: https://wiki.archlinux.org/index.php/Uncomplicated_Firewall 'Arch Wiki UFW'
[ufwdo]: https://www.digitalocean.com/community/tutorials/how-to-setup-a-firewall-with-ufw-on-an-ubuntu-and-debian-cloud-server 'Digital Ocean UFW'
[apparmorarchwiki]: https://wiki.archlinux.org/index.php/AppArmor 'The AppArmor Wiki From Arch'
[gnupg]: https://gnupg.org/ 'The GnuPG project'
[gnupgarchwiki]: https://wiki.archlinux.org/index.php/GnuPG 'Arch Wiki GnuPG'
[gnupgdo]: https://www.digitalocean.com/community/tutorials/how-to-use-gpg-to-encrypt-and-sign-messages 'GnuPG'
[opennic]: https://www.opennic.org/ 'The Open NIC project'
[opencrypt]: https://wiki.opennic.org/opennic/dnscrypt 'DNS Crypt'
[tor]: https://www.torproject.org/
[i2p]: https://geti2p.net/en/
[freenet]: https://freenetproject.org/pages/about.html
[protonvpn]: https://protonvpn.com/ 'Proton VPN'
[nordvpn]: https://nordvpn.com/ 'Nord VPN'
[piavpn]: https://www.privateinternetaccess.com/ 'PIA VPN'
[saaspass]: https://saaspass.com/
[keepass]: https://keepass.info/
[dnsdiag]: https://github.com/farrokhi/dnsdiag
[icecat]: https://www.gnu.org/software/gnuzilla/
