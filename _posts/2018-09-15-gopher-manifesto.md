---
layout: post
title: "The Gopher Manifesto"
date: 2018-09-15T04:52:43-07:00
author: RetroMe
summary: >
  Gopher is an infoserver which can deliver text, graphics, audio, and
  multimedia to clients. The frugal attitude to display, simplistic
  construction of sites, and lack of necessary investment in layout or design
  makes Gopher incredibly inexpensive to host and deploy. This is the Gopher
  manifesto.
categories: discussion
thumbnail: fa-desktop
tags:
 - gopher
 - port 70
---

# Gopher

Gopher is a protocol used for serving up text, graphics, audio, and multimedia
to clients. There is no graphical design and this means every page works the
same and it doesn't require any familiarity to move from Gopher hole to Gopher
hole. Gopher is simple! Gopher is the perfect replacement for the garbled mess
we now call the internet.

## The Manifesto

Bjorn Karger wrote the [Gopher Manifesto][manifesto] on November 16, 2000 and
it was Slashdotted on November 23, 2000. His treatise brought to light the fact
that Gopher is a viable alternative to a corporate dominated web that has
become increasingly commercialized and has lost much of the original spirit of
ingenuity and freedom of the early days. 

## The Modern Internet Is Dying

Today, we have Article 13 in Europe in which all sites must now use content
recognition technology to flag any material that resembles copyrighted
material. Do you like memes? You won't be able to post them soon because your
videos, text, music, photos, and essentially anything could potentially contain
copyrighted material in them and you will be breaking the law for showing them.
Made a remix or mix tape? Want to post streaming content on Twitch? You may
soon be unable to do so because of these laws being passed.

You can read more about [Article 13][effarticle13] thanks to the electronic
frontier foundation.

## What server?

Interested in getting started with Gopher hole hosting? Install
[Gophernicus][gserver] for a full-featured experience. You can be up and
running quickly and serving content without delay.

## Will Gopher Make A Come Back?

I believe it will in some ways. As the internet becomes increasingly hostile to
content creators I believe we will see a resurgence of Gopher use. I also
believe that combining Gopher with Tor could be a viable method to make Tor a
worthy privacy enhancing tool again. My ultimate goal however is to get Gopher
working with [Freenet][freenet].

[manifesto]: http://archive.is/McstR 'The Gopher Manifesto - WWW Version'
[effarticle13]: https://www.eff.org/deeplinks/2018/09/how-eus-copyright-filters-will-make-it-trivial-anyone-censor-internet 'The EFF on Article 13'
[gserver]: https://github.com/prologic/gophernicus 'A full-featured and secure gopher daemon'
[freenet]: https://freenetproject.org/ 'The Freenet Project'
